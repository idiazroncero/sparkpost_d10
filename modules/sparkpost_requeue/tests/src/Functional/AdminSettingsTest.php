<?php

namespace Drupal\Tests\sparkpost_requeue\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test basic functionality.
 *
 * @group sparkpost
 */
class AdminSettingsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['sparkpost', 'sparkpost_requeue'];

  /**
   * Regular user.
   *
   * @var false|object
   */
  protected $user;

  /**
   * Admin user.
   *
   * @var false|object
   */
  protected $admin;

  /**
   * {@inheritdoc}
   */
  public function setUp() : void {
    parent::setUp();
    $this->user = $this->drupalCreateUser();
    $this->admin = $this->drupalCreateUser([
      'administer sparkpost',
    ]);
  }

  /**
   * Access admin pages.
   */
  public function testAdminAccess() {
    $this->drupalLogin($this->user);

    // Try access sparkpost requeue admin form.
    $this->drupalGet('admin/config/services/sparkpost_requeue');
    $this->assertSession()->statusCodeEquals(403);

    // Login as admin.
    $this->drupalLogout();
    $this->drupalLogin($this->admin);

    // Try access sparkpost admin form.
    $this->drupalGet('admin/config/services/sparkpost_requeue');
    $this->assertSession()->statusCodeEquals(200);
  }

}
